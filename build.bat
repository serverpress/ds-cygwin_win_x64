::
:: Build the CYGWIN runtime folder for the given version number
::

:: Make temp folder if it does not exist
if not exist "temp" (
    mkdir temp
)

powershell -Command "& Invoke-WebRequest -OutFile .\setup.exe https://cygwin.com/setup-x86_64.exe"
.\setup.exe -q -n -N -d -B -R .\ -s https://mirrors.sonic.net/cygwin -l %cd%\temp -P wget
set PATH=%cd%\bin;%PATH%;
wget https://rawgit.com/transcode-open/apt-cyg/master/apt-cyg -P ./temp
install ./temp/apt-cyg /bin

:: Install our packages
bash apt-cyg install nano
bash apt-cyg install ncurses
bash apt-cyg install lftp
bash apt-cyg install subversion
bash apt-cyg install git
bash apt-cyg install sqlite3
bash apt-cyg install dos2unix
bash apt-cyg install unzip

:: Purge unnecessary files and folders
rm -rf .\bin\rnano
rm -rf .\bin\slogin
rm -rf .\bin\zipinfo
rm -rf .\dev\fd
rm -rf .\dev\stderr
rm -rf .\dev\stdin
rm -rf .\dev\stdout
rm -rf .\etc\hosts
rm -rf .\etc\mtab
rm -rf .\etc\networks
rm -rf .\etc\protocols
rm -rf .\etc\services
rm -rf .\etc\crypto-policies
rm -rf .\etc\pki\ca-trust\source\ca-bundle.legacy.crt
rm -rf .\usr\share\bash-completion\completions\svnadmin
rm -rf .\usr\share\bash-completion\completions\svndumpfilter
rm -rf .\usr\share\bash-completion\completions\svnlook
rm -rf .\usr\share\bash-completion\completions\svnsync
rm -rf .\usr\share\bash-completion\completions\svnversion
rm -rf .\usr\share\doc\git\html\index.html

:: Purge zero-byte files (git doesn't like 'em)
rm .\etc\pki\ca-trust\extracted\pem\objsign-ca-bundle.pem
rm .\usr\share\groff\1.22.4\tmac\mm\locale
rm .\usr\share\groff\1.22.4\tmac\mm\se_locale
rm .\usr\share\pki\ca-trust-legacy\ca-bundle.legacy.default.crt
rm .\usr\share\pki\ca-trust-legacy\ca-bundle.legacy.disable.crt
rm .\var\cache\rebase\rebase_dyn.old
rm .\var\cache\rebase\rebase_epoch
rm .\var\cache\rebase\rebase_user.old
rm .\var\cache\rebase\rebase_user_exe.old
rm .\var\lib\alternatives\.keep-alternatives
rm .\var\run\utmp

:: Purge empty directories (git doesn't like 'em either)
rm -rf .\dev\mqueue
rm -rf .\dev\shm
rm -rf .\etc\fstab.d
rm -rf .\etc\pki\ca-trust\source\anchors
rm -rf .\etc\pki\ca-trust\source\blacklist
rm -rf .\etc\pki\tls\misc
rm -rf .\etc\pki\tls\private
rm -rf .\etc\sasl2
rm -rf .\home
rm -rf .\usr\local\bin
rm -rf .\usr\local\etc
rm -rf .\usr\local\lib
rm -rf .\usr\share\pkgconfig
rm -rf .\usr\share\pki\ca-trust-source\anchors
rm -rf .\usr\share\pki\ca-trust-source\blacklist
rm -rf .\usr\src
rm -rf .\usr\tmp
rm -rf .\var\cache\man
rm -rf .\var\lib\rebase\user.d
rm -rf .\var\tmp

:: Patch files
copy /Y .\patch\nsswitch.conf .\etc\nsswitch.conf
